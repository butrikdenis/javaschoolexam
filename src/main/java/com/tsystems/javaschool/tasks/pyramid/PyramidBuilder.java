package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int heightOfPyramid = 0;
        int widthOfPyramid = 0;

        // Check if there is null inside the sequence
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        // Application to Triangle Numbers
        for (int i = 0; i < inputNumbers.size(); i++) {
            if ((i * (i + 1)) / 2 == inputNumbers.size()) {

                heightOfPyramid = i;
            }
        }
        // Throw exception if the size of inputNumbers is not a triangular number
        if (heightOfPyramid == 0) {
            throw new CannotBuildPyramidException();
        }
        // Calculate Width
        if (heightOfPyramid > 1) {
            widthOfPyramid = heightOfPyramid + (heightOfPyramid - 1);
        }
        // Bubble sort from smallest to largest
        boolean isSorted = false;
        int buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < inputNumbers.size() - 1; i++) {
                if (inputNumbers.get(i) > inputNumbers.get(i + 1)) {
                    isSorted = false;
                    buf = inputNumbers.get(i);
                    inputNumbers.set(i, inputNumbers.get(i + 1));
                    inputNumbers.set(i + 1, buf);
                }
            }
        }

        // Add 0 to ever case and showing
        int[][] pyramidArray = new int[heightOfPyramid][widthOfPyramid];
        for (int i = 0; i < heightOfPyramid; i++) {
            for (int j = 0; j < widthOfPyramid; j++) {
                pyramidArray[i][j] = 0;
            }
        }


        // Adding a sorted array to a zeros-filled 2d-array
        int countOfLine = 1; // сколько чисел будет в строке
        int indexOfArray = 0; // индекс массива
        for (int i = 0, shift = 0; i < heightOfPyramid; i++, shift++, countOfLine++) {
            int start = (widthOfPyramid / 2) - shift;
            for (int j = 0; j < countOfLine * 2; j += 2, indexOfArray++) {
                pyramidArray[i][start + j] = inputNumbers.get(indexOfArray);
            }
        }

        // Return 2D array
        return pyramidArray;
    }


}

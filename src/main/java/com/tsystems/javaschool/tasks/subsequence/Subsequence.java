package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int hitCounter = 0;
        // Test x and y for null
        if (x == null || y == null) {
            throw new IllegalArgumentException("Значение не может быть null");
        }
        // When x=0 and y>=0 than return "true"
        if (x.size() == 0 && y != null) {
            // System.out.println("Массив X равен нулю,а Y неравен NULL");
            return true;
        }
        // When y=0 and x>0 than return "false"
        if (x.size() != 0 && y.size() == 0) {
            return false;
        }
        // Test which X contains Y
        for (int i = 0; i < y.size(); i++) {
            if (y.get(i) == x.get(hitCounter)) {
                hitCounter++;
                if (hitCounter == x.size()) {
                    return true;
                }
            }
        }
        return false;
    }
}

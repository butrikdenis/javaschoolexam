package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {


    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */


    public String evaluate(String statement) {

        String testing = testingStatement(statement);
        String bracket = isEnoughBracket(testing);
        String prepared = preparingStatement(bracket);
        String rpn = statementToRPN(prepared);
        String answer = rpnToAnswer(rpn);
        return answer;
    }

    private int getPriority(char token) {
        // Prioritize Operations
        if (token == '*' || token == '/') {
            return 3;
        } else if (token == '+' || token == '-') {
            return 2;
        } else if (token == '(') {
            return 1;
        } else if (token == ')') {
            return -1;
        } else return 0;
    }

    private String isEnoughBracket(String statement) {
        // Check for the number of brackets
        if (statement == null) {
            return null;
        }
        int count = 0;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(') {
                count++;
            } else if (statement.charAt(i) == ')') {
                count--;
            }
        }
        if (statement.indexOf('(') > statement.indexOf(')')) {
            return null;
        }
        if (count != 0) {
            return null;
        }
        return statement;
    }

    private String testingStatement(String statement) {
        // Check the statement for the presence of null, "",commas, doubles or more "++", "--" and so on
        final String DIGIT = "0123456789";
        final String DIGITWITHBRACKET = "(0123456789)";

        if (statement == null || statement == "") {
            return null;
        }


        if (statement.contains(",")) {
            return null;
        }

        if (statement.contains(".")) {
            for (int i = 0; i < statement.length(); i++) {
                if (statement.charAt(i) == '.' && !DIGIT.contains(Character.toString(statement.charAt(i + 1)))) {
                    return null;
                } else if (statement.charAt(i) == '.' && !DIGIT.contains(Character.toString(statement.charAt(i - 1)))) {
                    return null;
                }
            }
        }

        if (statement.contains("+") || statement.contains("-") || statement.contains("*") || statement.contains("/")) {
            for (int i = 0; i < statement.length(); i++) {
                if (statement.charAt(i) == '+' && !DIGITWITHBRACKET.contains(Character.toString(statement.charAt(i + 1)))) {
                    return null;
                } else if (statement.charAt(i) == '+' && !DIGITWITHBRACKET.contains(Character.toString(statement.charAt(i - 1)))) {
                    return null;
                }
                if (statement.charAt(i) == '-' && !DIGITWITHBRACKET.contains(Character.toString(statement.charAt(i + 1)))) {
                    return null;
                } else if (statement.charAt(i) == '-' && !DIGITWITHBRACKET.contains(Character.toString(statement.charAt(i - 1)))) {
                    return null;
                }
                if (statement.charAt(i) == '*' && !DIGITWITHBRACKET.contains(Character.toString(statement.charAt(i + 1)))) {
                    return null;
                } else if (statement.charAt(i) == '*' && !DIGITWITHBRACKET.contains(Character.toString(statement.charAt(i - 1)))) {
                    return null;
                }
                if (statement.charAt(i) == '/' && !DIGITWITHBRACKET.contains(Character.toString(statement.charAt(i + 1)))) {
                    return null;
                } else if (statement.charAt(i) == '/' && !DIGITWITHBRACKET.contains(Character.toString(statement.charAt(i - 1)))) {
                    return null;
                }
            }

        }

        return statement;
    }

    private String preparingStatement(String statement) {
        // Prepare an statement if there are negative numbers
        if (statement == null) {
            return null;
        }


        String preparedExpression = "";
        for (int i = 0; i < statement.length(); i++) {
            char symbol = statement.charAt(i);
            if (symbol == '-') {
                if (i == 0) {
                    preparedExpression += '0';
                } else if (statement.charAt(i - 1) == '(') {
                    preparedExpression += '0';
                }
            }
            preparedExpression += symbol;
        }
        return preparedExpression;
    }

    private String statementToRPN(String statement) {
        // Translate the expression in the reverse polish notation

        if (statement == null) {
            return null;
        }
        String current = "";
        Stack<Character> stack = new Stack<>();
        int priority;
        int haveOpenBracket = 0;

        for (int i = 0; i < statement.length(); i++) {
            priority = getPriority(statement.charAt(i));

            if (priority == 0) {
                current += statement.charAt(i);

            }
            if (priority == 1) {
                stack.push(statement.charAt(i));
                haveOpenBracket++;


            }
            if (priority > 1) {
                current += ' ';
                while (!stack.empty()) {
                    if (getPriority(stack.peek()) >= priority) {
                        current += stack.pop();

                    } else break;
                }
                stack.push(statement.charAt(i));
            }

            if (priority == -1) {
                current += ' ';
                while (getPriority(stack.peek()) != 1) {
                    current += stack.pop();
                    haveOpenBracket--;
                }
                stack.pop();
            }

//            System.out.println(current);
//            System.out.println(stack);
        }

        while (!stack.empty()) {
//            if (stack.size() == 1) {
//                if (stack.peek() == '(') {
//                    current = null;
//                }
//            }
            current += stack.pop();
        }
//        System.out.println("Количество незакрытых скобок: " + haveOpenBracket);
        if (haveOpenBracket != 0) {
            return null;
        }

        return current;
    }

    private String rpnToAnswer(String statement) {
        // Process the expression, calculate the answer
        if (statement == null) {
            return null;
        }

        String operand = "";

        Stack<Double> stack = new Stack<>();

        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == ' ') {
                continue;
            }
            if (getPriority(statement.charAt(i)) == 0) {
                while (statement.charAt(i) != ' ' && getPriority(statement.charAt(i)) == 0) {
                    operand += statement.charAt(i++);
                    if (i == statement.length()) {
                        break;
                    }
                }
                stack.push(Double.parseDouble(operand));
                operand = "";
            }

            if (getPriority(statement.charAt(i)) > 1) {
                double a = stack.pop();
                double b = stack.pop();

                if (statement.charAt(i) == '+') {
                    stack.push(b + a);
                }
                if (statement.charAt(i) == '-') {
                    stack.push(b - a);
                }
                if (statement.charAt(i) == '*') {
                    stack.push(b * a);
                }
                if (statement.charAt(i) == '/') {
                    stack.push(b / a);
                }

            }

        }
        String answer = "";

        double amount = stack.pop();


        String s = Double.toString(amount);
        if (amount == Double.POSITIVE_INFINITY) {
            answer = null;
        } else {


            int countZeroAfterDot = 0;
            for (int i = s.indexOf('.'); i < s.length(); i++) {
                if (s.charAt(i) == '0') {
                    countZeroAfterDot++;

                }
            }


            if (countZeroAfterDot == s.length() - s.indexOf('.') - 1) {

                for (int i = 0; i < s.indexOf('.'); i++) {
                    answer += s.charAt(i);
                }
            } else answer = s;

        }
        return answer;
    }

}
